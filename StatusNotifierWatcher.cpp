/**
 * Copyright (c) 2022
 *    Marcus Britanicus (https://gitlab.com/marcusbritanicus)
 *
 * This file was originally taken from LXQt project
 * <https://github.com/lxqt/lxqt-panel>
 * Several modifications have been made to suit the needs of DFL.
 * Any and all bug reports are to be filed with DFL and not LXQt.
 *
 * This library implements the StatusNotifierItem specifications.
 * https://www.freedesktop.org/wiki/Specifications/StatusNotifierItem/
 * DFL::StatusNotifierWatcher class provides the code for the watcher.
 **/

#include <QtCore>
#include <QDebug>
#include <QDBusConnectionInterface>

#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <cstdio>

#include "SNITypes.hpp"
#include "StatusNotifierWatcher.hpp"

DFL::StatusNotifierWatcher::StatusNotifierWatcher( QObject *parent ) : QObject( parent ) {
    qRegisterMetaType<DFL::SNI::IconPixmap>( "IconPixmap" );
    qDBusRegisterMetaType<DFL::SNI::IconPixmap>();
    qRegisterMetaType<DFL::SNI::IconPixmapList>( "IconPixmapList" );
    qDBusRegisterMetaType<DFL::SNI::IconPixmapList>();
    qRegisterMetaType<DFL::SNI::ToolTip>( "ToolTip" );
    qDBusRegisterMetaType<DFL::SNI::ToolTip>();

    QDBusConnection dbus = QDBusConnection::sessionBus();

    serviceRunning   = dbus.registerService( "org.kde.StatusNotifierWatcher" );
    objectRegistered = dbus.registerObject( "/StatusNotifierWatcher", this, QDBusConnection::ExportScriptableContents );

    mWatcher = new QDBusServiceWatcher( this );
    mWatcher->setConnection( dbus );
    mWatcher->setWatchMode( QDBusServiceWatcher::WatchForUnregistration );

    connect( mWatcher, &QDBusServiceWatcher::serviceUnregistered, this, &DFL::StatusNotifierWatcher::serviceUnregistered );
}


DFL::StatusNotifierWatcher::~StatusNotifierWatcher() {
    QDBusConnection::sessionBus().unregisterService( "org.kde.StatusNotifierWatcher" );
}


bool DFL::StatusNotifierWatcher::isServiceRunning() {
    return serviceRunning;
}


bool DFL::StatusNotifierWatcher::isObjectRegistered() {
    return objectRegistered;
}


void DFL::StatusNotifierWatcher::RegisterStatusNotifierItem( const QString& serviceOrPath ) {
    QString service = serviceOrPath;
    QString path    = "/StatusNotifierItem";

    // workaround for sni-qt
    if ( service.startsWith( '/' ) ) {
        path    = service;
        service = message().service();
    }

    QString notifierItemId = service + path;

    if ( QDBusConnection::sessionBus().interface()->isServiceRegistered( service ).value() && !mServices.contains( notifierItemId ) ) {
        mServices << notifierItemId;
        mWatcher->addWatchedService( service );
        emit StatusNotifierItemRegistered( notifierItemId );
    }
}


void DFL::StatusNotifierWatcher::RegisterStatusNotifierHost( const QString& service ) {
    if ( !mHosts.contains( service ) ) {
        mHosts.append( service );
        mWatcher->addWatchedService( service );
    }
}


void DFL::StatusNotifierWatcher::serviceUnregistered( const QString& service ) {
    mWatcher->removeWatchedService( service );

    if ( mHosts.contains( service ) ) {
        mHosts.removeAll( service );
        return;
    }

    QString               match = service + '/';
    QStringList::Iterator it    = mServices.begin();

    while ( it != mServices.end() ) {
        if ( it->startsWith( match ) ) {
            QString name = *it;
            it = mServices.erase( it );
            emit StatusNotifierItemUnregistered( name );
        }

        else {
            ++it;
        }
    }
}
