# DFL SNI
Status Notifier Interface for DFL. This project provides SN Watcher class that registers the Status icons and the  Host.


### Dependencies:
* <tt>Qt5     (qtbase5-dev, qtbase5-dev-tools)</tt>
* <tt>meson   (For configuring the project)</tt>
* <tt>ninja   (To build the project)</tt>


### Notes for compiling (Qt5) - linux:

- Install all the dependencies
- Download the sources
  * Git: `git clone https://gitlab.com/desktop-frameworks/status-notifier.git dfl-sni`
- Enter the `dfl-sni` folder
  * `cd dfl-sni`
- Configure the project - we use meson for project management
  * `meson .build --prefix=/usr --buildtype=release`
- Compile and install - we use ninja
  * `ninja -C .build -k 0 -j $(nproc) && sudo ninja -C .build install`


### Known Bugs
* Please test and let us know


### Upcoming
* Any other feature you request for... :)
