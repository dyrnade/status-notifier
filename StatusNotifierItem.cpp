/**
 * Copyright (c) 2022
 *    Marcus Britanicus (https://gitlab.com/marcusbritanicus)
 *
 * This file was originally taken from LXQt project
 * <https://github.com/lxqt/lxqt-panel>
 * Several modifications have been made to suit the needs of DFL.
 * Any and all bug reports are to be filed with DFL and not LXQt.
 *
 * This library implements the StatusNotifierInterface specifications.
 * https://www.freedesktop.org/wiki/Specifications/StatusNotifierInterface/
 * DFL::StatusNotifierWatcher class provides the code for the watcher.
 *
 * Impl Class: Performs all DBus Actions Asynchronously.
 **/

#include "StatusNotifierItem.hpp"
#include "SNIInterface.hpp"

DFL::StatusNotifierItem::StatusNotifierItem( QString serv, QString path, QDBusConnection conn, QObject *parent ) : QObject( parent ) {
    mSni = new StatusNotifierInterface( serv, path, conn, parent );

    //forward StatusNotifierInterface signals
    connect( mSni, &StatusNotifierInterface::NewAttentionIcon, this, &DFL::StatusNotifierItem::NewAttentionIcon );
    connect( mSni, &StatusNotifierInterface::NewIcon,          this, &DFL::StatusNotifierItem::NewIcon );
    connect( mSni, &StatusNotifierInterface::NewOverlayIcon,   this, &DFL::StatusNotifierItem::NewOverlayIcon );
    connect( mSni, &StatusNotifierInterface::NewStatus,        this, &DFL::StatusNotifierItem::NewStatus );
    connect( mSni, &StatusNotifierInterface::NewTitle,         this, &DFL::StatusNotifierItem::NewTitle );
    connect( mSni, &StatusNotifierInterface::NewToolTip,       this, &DFL::StatusNotifierItem::NewToolTip );
}


DFL::StatusNotifierItem::~StatusNotifierItem() {
    delete mSni;
}


QDBusPendingReply<QDBusVariant> DFL::StatusNotifierItem::asyncPropGet( QString const& property ) {
    QDBusMessage msg = QDBusMessage::createMethodCall(
        mSni->service(),
        mSni->path(),
        QLatin1String( "org.freedesktop.DBus.Properties" ),
        QLatin1String( "Get" )
    );

    msg << mSni->interface() << property;
    return mSni->connection().asyncCall( msg );
}


QDBusPendingReply<> DFL::StatusNotifierItem::Activate( int x, int y ) {
    return mSni->Activate( x, y );
}


QDBusPendingReply<> DFL::StatusNotifierItem::ContextMenu( int x, int y ) {
    return mSni->ContextMenu( x, y );
}


QDBusPendingReply<> DFL::StatusNotifierItem::Scroll( int delta, const QString& orientation ) {
    return mSni->Scroll( delta, orientation );
}


QDBusPendingReply<> DFL::StatusNotifierItem::SecondaryActivate( int x, int y ) {
    return mSni->SecondaryActivate( x, y );
}


QString DFL::StatusNotifierItem::service() const {
    return mSni->service();
}
