/**
 * Copyright (c) 2022
 *    Marcus Britanicus (https://gitlab.com/marcusbritanicus)
 *
 * This file was originally taken from LXQt project
 * <https://github.com/lxqt/lxqt-panel>
 * Several modifications have been made to suit the needs of DFL.
 * Any and all bug reports are to be filed with DFL and not LXQt.
 *
 * This library implements the StatusNotifierItem specifications.
 * https://www.freedesktop.org/wiki/Specifications/StatusNotifierItem/
 * DFL::StatusNotifierWatcher class provides the code for the watcher.
 **/

#pragma once

#include <QDBusConnection>
#include <QDBusContext>
#include <QDBusMessage>
#include <QDBusMetaType>
#include <QDBusServiceWatcher>

namespace DFL {
    class StatusNotifierWatcher;
}

class DFL::StatusNotifierWatcher : public QObject, protected QDBusContext {
    Q_OBJECT
    Q_CLASSINFO( "D-Bus Interface", "org.kde.StatusNotifierWatcher" )
    Q_SCRIPTABLE Q_PROPERTY( bool IsStatusNotifierHostRegistered READ isStatusNotifierHostRegistered )
    Q_SCRIPTABLE Q_PROPERTY( int ProtocolVersion READ protocolVersion )
    Q_SCRIPTABLE Q_PROPERTY( QStringList RegisteredStatusNotifierItems READ RegisteredStatusNotifierItems )

    public:
        explicit StatusNotifierWatcher( QObject *parent = nullptr );

        ~StatusNotifierWatcher();

        bool isStatusNotifierHostRegistered() { return mHosts.count() > 0; }
        int protocolVersion() const { return 0; }
        QStringList RegisteredStatusNotifierItems() const { return mServices; }

        bool isServiceRunning();
        bool isObjectRegistered();

    signals:
        Q_SCRIPTABLE void StatusNotifierItemRegistered( const QString& service );
        Q_SCRIPTABLE void StatusNotifierItemUnregistered( const QString& service );
        Q_SCRIPTABLE void StatusNotifierHostRegistered();

    public slots:
        Q_SCRIPTABLE void RegisterStatusNotifierItem( const QString& serviceOrPath );
        Q_SCRIPTABLE void RegisterStatusNotifierHost( const QString& service );

        void serviceUnregistered( const QString& service );

    private:
        QStringList mServices;
        QStringList mHosts;
        QDBusServiceWatcher *mWatcher;

        bool serviceRunning;
        bool objectRegistered;
};
