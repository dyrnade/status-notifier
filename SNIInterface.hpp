/**
 * Copyright (c) 2022
 *    Marcus Britanicus (https://gitlab.com/marcusbritanicus)
 *
 * This file was originally taken from LXQt project
 * <https://github.com/lxqt/lxqt-panel>
 * Several modifications have been made to suit the needs of DFL.
 * Any and all bug reports are to be filed with DFL and not LXQt.
 *
 * This library implements the StatusNotifierItem specifications.
 * https://www.freedesktop.org/wiki/Specifications/StatusNotifierItem/
 * DFL::StatusNotifierWatcher class provides the code for the watcher.
 *
 * Impl Class: Performs all DBus Actions Asynchronously.
 **/

#pragma once

#include <QObject>
#include <QByteArray>
#include <QList>
#include <QMap>
#include <QString>
#include <QStringList>
#include <QVariant>
#include <QtDBus>

#include "SNITypes.hpp"

/*
 * Proxy class for interface org.kde.StatusNotifierItem
 */
class StatusNotifierInterface : public QDBusAbstractInterface {
    Q_OBJECT;

    public:
        static inline const char *staticInterfaceName() {
            return "org.kde.StatusNotifierItem";
        }

        StatusNotifierInterface( QString service, QString path, QDBusConnection connection, QObject *parent = nullptr );

        ~StatusNotifierInterface();

        Q_PROPERTY( QString AttentionIconName READ attentionIconName )
        QString attentionIconName() const;

        Q_PROPERTY( DFL::SNI::IconPixmapList AttentionIconPixmap READ attentionIconPixmap )
        DFL::SNI::IconPixmapList attentionIconPixmap() const;

        Q_PROPERTY( QString AttentionMovieName READ attentionMovieName )
        QString attentionMovieName() const;

        Q_PROPERTY( QString Category READ category )
        QString category() const;

        Q_PROPERTY( QString IconName READ iconName )
        QString iconName() const;

        Q_PROPERTY( DFL::SNI::IconPixmapList IconPixmap READ iconPixmap )
        DFL::SNI::IconPixmapList iconPixmap() const;

        Q_PROPERTY( QString IconThemePath READ iconThemePath )
        QString iconThemePath() const;

        Q_PROPERTY( QString Id READ id )
        QString id() const;

        Q_PROPERTY( bool ItemIsMenu READ itemIsMenu )
        bool itemIsMenu() const;

        Q_PROPERTY( QDBusObjectPath Menu READ menu )
        QDBusObjectPath menu() const;

        Q_PROPERTY( QString OverlayIconName READ overlayIconName )
        QString overlayIconName() const;

        Q_PROPERTY( DFL::SNI::IconPixmapList OverlayIconPixmap READ overlayIconPixmap )
        DFL::SNI::IconPixmapList overlayIconPixmap() const;

        Q_PROPERTY( QString Status READ status )
        QString status() const;

        Q_PROPERTY( QString Title READ title )
        QString title() const;

        Q_PROPERTY( DFL::SNI::ToolTip ToolTip READ toolTip )
        DFL::SNI::ToolTip toolTip() const;

        Q_PROPERTY( int WindowId READ windowId )
        int windowId() const;

    public Q_SLOTS: // METHODS
        QDBusPendingReply<> Activate( int x, int y );
        QDBusPendingReply<> ContextMenu( int x, int y );
        QDBusPendingReply<> Scroll( int delta, const QString& orientation );
        QDBusPendingReply<> SecondaryActivate( int x, int y );

    Q_SIGNALS: // SIGNALS
        void NewAttentionIcon();
        void NewIcon();
        void NewOverlayIcon();
        void NewStatus( const QString& status );
        void NewTitle();
        void NewToolTip();
};
