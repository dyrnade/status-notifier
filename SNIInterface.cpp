/**
 * Copyright (c) 2022
 *    Marcus Britanicus (https://gitlab.com/marcusbritanicus)
 *
 * This file was originally taken from LXQt project
 * <https://github.com/lxqt/lxqt-panel>
 * Several modifications have been made to suit the needs of DFL.
 * Any and all bug reports are to be filed with DFL and not LXQt.
 *
 * This library implements the StatusNotifierInterface specifications.
 * https://www.freedesktop.org/wiki/Specifications/StatusNotifierInterface/
 * DFL::StatusNotifierWatcher class provides the code for the watcher.
 *
 * Impl Class: Performs all DBus Actions Asynchronously.
 **/

#include "SNIInterface.hpp"

StatusNotifierInterface::StatusNotifierInterface( QString serv, QString path, QDBusConnection conn, QObject *parent ) :
    QDBusAbstractInterface( serv, path, staticInterfaceName(), conn, parent ) {
}


StatusNotifierInterface::~StatusNotifierInterface() {
}


QString StatusNotifierInterface::attentionIconName() const {
    return qvariant_cast<QString>( property( "AttentionIconName" ) );
}


DFL::SNI::IconPixmapList StatusNotifierInterface::attentionIconPixmap() const {
    return qvariant_cast<DFL::SNI::IconPixmapList>( property( "AttentionIconPixmap" ) );
}


QString StatusNotifierInterface::attentionMovieName() const {
    return qvariant_cast<QString>( property( "AttentionMovieName" ) );
}


QString StatusNotifierInterface::category() const {
    return qvariant_cast<QString>( property( "Category" ) );
}


QString StatusNotifierInterface::iconName() const {
    return qvariant_cast<QString>( property( "IconName" ) );
}


DFL::SNI::IconPixmapList StatusNotifierInterface::iconPixmap() const {
    return qvariant_cast<DFL::SNI::IconPixmapList>( property( "IconPixmap" ) );
}


QString StatusNotifierInterface::iconThemePath() const {
    return qvariant_cast<QString>( property( "IconThemePath" ) );
}


QString StatusNotifierInterface::id() const {
    return qvariant_cast<QString>( property( "Id" ) );
}


bool StatusNotifierInterface::itemIsMenu() const {
    return qvariant_cast<bool>( property( "ItemIsMenu" ) );
}


QDBusObjectPath StatusNotifierInterface::menu() const {
    return qvariant_cast<QDBusObjectPath>( property( "Menu" ) );
}


QString StatusNotifierInterface::overlayIconName() const {
    return qvariant_cast<QString>( property( "OverlayIconName" ) );
}


DFL::SNI::IconPixmapList StatusNotifierInterface::overlayIconPixmap() const {
    return qvariant_cast<DFL::SNI::IconPixmapList>( property( "OverlayIconPixmap" ) );
}


QString StatusNotifierInterface::status() const {
    return qvariant_cast<QString>( property( "Status" ) );
}


QString StatusNotifierInterface::title() const {
    return qvariant_cast<QString>( property( "Title" ) );
}


DFL::SNI::ToolTip StatusNotifierInterface::toolTip() const {
    return qvariant_cast<DFL::SNI::ToolTip>( property( "ToolTip" ) );
}


int StatusNotifierInterface::windowId() const {
    return qvariant_cast<int>( property( "WindowId" ) );
}


QDBusPendingReply<> StatusNotifierInterface::Activate( int x, int y ) {
    QList<QVariant> argumentList;

    argumentList << QVariant::fromValue( x ) << QVariant::fromValue( y );
    return asyncCallWithArgumentList( QLatin1String( "Activate" ), argumentList );
}


QDBusPendingReply<> StatusNotifierInterface::ContextMenu( int x, int y ) {
    QList<QVariant> argumentList;

    argumentList << QVariant::fromValue( x ) << QVariant::fromValue( y );
    return asyncCallWithArgumentList( QLatin1String( "ContextMenu" ), argumentList );
}


QDBusPendingReply<> StatusNotifierInterface::Scroll( int delta, const QString& orientation ) {
    QList<QVariant> argumentList;

    argumentList << QVariant::fromValue( delta ) << QVariant::fromValue( orientation );
    return asyncCallWithArgumentList( QLatin1String( "Scroll" ), argumentList );
}


QDBusPendingReply<> StatusNotifierInterface::SecondaryActivate( int x, int y ) {
    QList<QVariant> argumentList;

    argumentList << QVariant::fromValue( x ) << QVariant::fromValue( y );
    return asyncCallWithArgumentList( QLatin1String( "SecondaryActivate" ), argumentList );
}
