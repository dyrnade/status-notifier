/**
 * Copyright (c) 2022
 *    Marcus Britanicus (https://gitlab.com/marcusbritanicus)
 *
 * This file was originally taken from LXQt project
 * <https://github.com/lxqt/lxqt-panel>
 * Several modifications have been made to suit the needs of DFL.
 * Any and all bug reports are to be filed with DFL and not LXQt.
 *
 * This library implements the StatusNotifierItem specifications.
 * https://www.freedesktop.org/wiki/Specifications/StatusNotifierItem/
 * DFL::StatusNotifierWatcher class provides the code for the watcher.
 **/

#include "SNITypes.hpp"

// Marshall the IconPixmap data into a D-Bus argument
QDBusArgument &operator<<( QDBusArgument& argument, const DFL::SNI::IconPixmap& icon ) {
    argument.beginStructure();
    argument << icon.width;
    argument << icon.height;
    argument << icon.bytes;
    argument.endStructure();
    return argument;
}


// Retrieve the ImageStruct data from the D-Bus argument
const QDBusArgument &operator>>( const QDBusArgument& argument, DFL::SNI::IconPixmap& icon ) {
    argument.beginStructure();
    argument >> icon.width;
    argument >> icon.height;
    argument >> icon.bytes;
    argument.endStructure();
    return argument;
}


// Marshall the ToolTip data into a D-Bus argument
QDBusArgument &operator<<( QDBusArgument& argument, const DFL::SNI::ToolTip& toolTip ) {
    argument.beginStructure();
    argument << toolTip.iconName;
    argument << toolTip.iconPixmap;
    argument << toolTip.title;
    argument << toolTip.description;
    argument.endStructure();
    return argument;
}


// Retrieve the ToolTip data from the D-Bus argument
const QDBusArgument &operator>>( const QDBusArgument& argument, DFL::SNI::ToolTip& toolTip ) {
    argument.beginStructure();
    argument >> toolTip.iconName;
    argument >> toolTip.iconPixmap;
    argument >> toolTip.title;
    argument >> toolTip.description;
    argument.endStructure();
    return argument;
}
